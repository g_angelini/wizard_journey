﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DialogueManager : MonoBehaviour
{

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public Animator animator;

    [Header("Gameplay Elements")]
    public Movement player;
    public GameObject btn1;
    public GameObject btn2;
    public GameObject btn3;


    private Queue<string> sentences;

    // Use this for initializatio
    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        DeactivateGameplayElements();
        animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    private void DeactivateGameplayElements()
    {
        player.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.enabled = false;
        btn1.SetActive(false);
        btn2.SetActive(false);
        btn3.SetActive(false);
    }

    private void ActivateGameplayElements()
    {
        player.enabled = true;
        btn1.SetActive(true);
        btn2.SetActive(true);
        btn3.SetActive(true);
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        ActivateGameplayElements();
    }

}