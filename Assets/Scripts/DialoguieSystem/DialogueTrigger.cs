﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public bool autodestroyAfterDialogue = true;
    public Dialogue dialogue;

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        AudioManager.instance.Play("Fairy");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            TriggerDialogue();
            if (autodestroyAfterDialogue)
            {
                Destroy(gameObject);
            }
        }
    }

}