﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }

    public void Play(string name)
    {
        Sound sToPlay = Array.Find(sounds, sound => sound.name == name);
        if (sToPlay == null)
        {
            Debug.LogWarning("Sound of name: " + name + " not found!");
            return;
        }
        sToPlay.source.Play();
    }

    public bool isPlaying(string name)
    {
        Sound sToPlay = Array.Find(sounds, sound => sound.name == name);
        if (sToPlay == null)
        {
            Debug.LogWarning("Sound of name: " + name + " not found!");
        }

        return sToPlay.source.isPlaying; ;
    }

    public void PlayOneShot(string name)
    {
        Sound sToPlay = Array.Find(sounds, sound => sound.name == name);
        if (sToPlay == null)
        {
            Debug.LogWarning("Sound of name: " + name + " not found!");
            return;
        }
        sToPlay.source.PlayOneShot(sToPlay.clip);
    }

    public void Stop(string name)
    {
        Sound sToStop = Array.Find(sounds, sound => sound.name == name);
        if (sToStop == null)
        {
            Debug.LogWarning("Sound of name: " + name + " not found!");
            return;
        }
        sToStop.source.Stop();
    }
}
