﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cart : MonoBehaviour, IInteractable
{
    public GameObject interactUI;
    public GameObject player;
    public Animator animator;

    private void Start()
    {
        //interactUI.transform.position = Camera.main.WorldToScreenPoint(transform.position + Vector3.up);
    }

    public void Interact()
    {
        player.transform.parent = transform;
        player.GetComponent<Rigidbody2D>().isKinematic = true;
        player.transform.position = transform.GetChild(0).position;
        animator.enabled = true;
        AudioManager.instance.Play("Cart");
    }

    public void TurnOffInteractUI()
    {
        interactUI.SetActive(false);
    }

    public void TurnOnInteractUI()
    {
        interactUI.SetActive(true);
    }
}
