﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour, ICollectable
{
    public void DisappearFromScreen()
    {
        AudioManager.instance.Play("Page");
        Destroy(gameObject);
    }
}
