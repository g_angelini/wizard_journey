﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpecialAbility : MonoBehaviour
{
    [SerializeField] float manaDecreaseTime = 5f; // time to empty mana
    [SerializeField] float manaRecoverTime = 2f; // time to recover mana when completely empty
    [SerializeField] float TimeForEmptyManaRecovery = 3f; // punishement time if mana gets empty
    [Range(0f, 1f)] [SerializeField] float manaSliderSmoothnes = 0.2f; //Higer less smooth, lower more smooth
    public GameObject abilityBlock; //Block prefab
    public Slider slider; //slider
    public Sprite manaSprite;
    Sprite defaultSprite;
    bool activeState = false;
    float manaDecreaseInterval; // interval of time between decrease
    float manaIncreaseInterval; // interval of time between increase

    // Start is called before the first frame update
    void Start()
    {
        defaultSprite = GetComponent<SpriteRenderer>().sprite;
        slider.value = slider.maxValue;
        manaDecreaseInterval = manaDecreaseTime / (slider.maxValue * manaSliderSmoothnes);
        manaIncreaseInterval = manaRecoverTime / (slider.maxValue * manaSliderSmoothnes);
    }

    // Update is called once per frame
    void Update()
    {
        if (activeState)
        {
            PlaceAbilityBlock();
            CheckMana();
        }
    }


    public void AbilityOnOff() //To set OnClick if ability off than sets it on, else ability off
    {
        if (activeState)
        {
            DeactivateAbility();
        }
        else
        {
            ActivateAbility();
        }

    }

    private void ActivateAbility()
    {
        AudioManager.instance.Play("ActivateAbility");
        GetComponent<SpriteRenderer>().sprite = manaSprite;
        activeState = true;
        StartCoroutine(ManaDecreaseOverTime());
    }

    IEnumerator ManaDecreaseOverTime()
    {
        if (slider.value > 0 && activeState)
        {
            slider.value -= manaSliderSmoothnes;
            yield return new WaitForSeconds(manaDecreaseInterval);
            StartCoroutine(ManaDecreaseOverTime());
        }
    }

    void DeactivateAbility()
    {
        AudioManager.instance.Play("DeactivateAbility");
        GetComponent<SpriteRenderer>().sprite = defaultSprite;
        activeState = false;
        abilityBlock.SetActive(false);
        StartCoroutine(ManaRecover());
    }

    void PlaceAbilityBlock()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId) && touch.phase == TouchPhase.Began)
                {
                    abilityBlock.transform.position = Camera.main.ScreenToWorldPoint(touch.position) + new Vector3(0f, 0f, 10f);
                    abilityBlock.SetActive(true);
                }

            }
        }
    }

    void CheckMana()
    {
        if (slider.value <= 0)
        {
            activeState = false;
            abilityBlock.SetActive(false);
            Invoke("InvokeManaRecover", TimeForEmptyManaRecovery);
        }
    }

    void InvokeManaRecover()
    {
        StartCoroutine(ManaRecover());
    }

    IEnumerator ManaRecover()
    {
        if (slider.value < slider.maxValue)
        {
            slider.value += manaSliderSmoothnes;
            yield return new WaitForSeconds(manaIncreaseInterval);
            StartCoroutine(ManaRecover());
        }
    }
}
