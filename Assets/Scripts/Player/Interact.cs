﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    IInteractable interactableObj;
    bool canInteract;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<IInteractable>() != null)
        {
            collision.gameObject.GetComponent<IInteractable>().TurnOnInteractUI();
            interactableObj = collision.gameObject.GetComponent<IInteractable>();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(interactableObj != null)
        {
            interactableObj.TurnOffInteractUI();
            interactableObj = null;
        }
    }


    public void InteractOnClick()
    {
        if(interactableObj != null)
        {
            interactableObj.Interact();
        } else
        {
            AudioManager.instance.Play("Cant");
        }
    }
}
