﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollectObjects : MonoBehaviour
{
    public TextMeshProUGUI pageCounterTXT;
    int pageCounter;

    private void Start()
    {
        pageCounter = 0;
        pageCounterTXT.text = pageCounter.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<ICollectable>() != null)
        {
            pageCounter++;
            pageCounterTXT.text = pageCounter.ToString();
            collision.gameObject.GetComponent<ICollectable>().DisappearFromScreen();
        }
    }
}
