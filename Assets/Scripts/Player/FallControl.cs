﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallControl : MonoBehaviour
{
    //[SerializeField] float maxFallSpeed = 20f;
    [SerializeField] float peakFallMultiplier = 2.5f;
    [SerializeField] float lowFallMultiplier = 2f;
    public GetUIButton jumpButton;
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (peakFallMultiplier - 1) * Time.fixedDeltaTime;
        }
        else if (rb.velocity.y > 0 && !jumpButton.buttonPressed)
        {
            rb.velocity += Vector2.up * Physics2D.gravity * (lowFallMultiplier - 1) * Time.fixedDeltaTime;
        }
    }
}
