﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float velocity = 10f;
    public CharacterController2D controller;
    public FloatingJoystick joystick;
    public GetUIButton jumpButton;
    public SpriteRenderer sprite;
    bool jumpBtnUp;
    bool jump = false;
    Vector2 input;
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        MyInput();
        JumpImput();
    }

    private void JumpImput()
    {
        if (jumpButton.buttonPressed && jumpBtnUp)
        {
            
            jump = true;
        }
        jumpBtnUp = !jumpButton.buttonPressed;
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        controller.Move(input.x * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    private void MyInput()
    {
        if (Mathf.Abs(joystick.Horizontal) > 0.1)
        {
            input.x = joystick.Horizontal*velocity;
        }
        else
        {
            input.x = 0f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        bool upCollision = false;
        foreach(ContactPoint2D contact in collision.contacts)
        {
            if(Vector2.Angle(contact.normal, Vector2.up) == 0)
            {
                upCollision = true;
            }
        }

        if (upCollision)
        {
            AudioManager.instance.Play("Land");
        }
    }
}
