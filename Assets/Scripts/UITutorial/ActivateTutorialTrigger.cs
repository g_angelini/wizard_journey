﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTutorialTrigger : MonoBehaviour
{
    public GameObject dialogueTrigger;
    bool isDeactivated = true;
    TriggerUITutorialObj tutorialToActivate;

    private void Start()
    {
        tutorialToActivate = GetComponent<TriggerUITutorialObj>();
        tutorialToActivate.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(dialogueTrigger == null && isDeactivated)
        {
            isDeactivated = true;
            tutorialToActivate.enabled = true;
        }
    }
}
