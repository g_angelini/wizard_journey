﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUITutorialObj : MonoBehaviour
{
    public GameObject UITutorialObj;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        UITutorialObj.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        UITutorialObj.SetActive(false);
    }
}
