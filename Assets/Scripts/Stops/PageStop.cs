﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PageStop : MonoBehaviour
{
    public Transform player;
    public TextMeshProUGUI pageCounter;
    public GameObject dialoguePageCollected;
    public GameObject dialoguePageMissing;
    public GameObject ColliderStop;

    bool actionTriggered = false;


    private void Update()
    {
        if (pageCounter.text.Equals("3") && !actionTriggered)
        {
            actionTriggered = true;
            dialoguePageCollected.GetComponent<DialogueTrigger>().TriggerDialogue();
            dialoguePageMissing.SetActive(false);
            Destroy(ColliderStop);
        }
    }
}
